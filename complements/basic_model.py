import numpy as np
import tensorflow as tf
import os



class model:
    """
    Default Neural Network model

    Characteristics:

        - Contain 1 additional layer with h neurons.
        - Parameters are initialised with Xavier's method
        - Cost function is MSE
        - Training is done with AdamOptimizer.

    """

    def __init__(self, features, nb_neurons=10, learn_rate=1e-3, index=0, reuse=False, save=False):

        self.dimension = features.shape[1]
        self.nb_neurons = nb_neurons
        self.learn_rate = learn_rate
        self.index = index
        self.reuse = reuse
        self.error_track = []
        self.min = np.inf

        self.input = tf.placeholder(tf.float32, [None, self.dimension], name="inp")
        self.output = tf.placeholder(tf.float32, [None, 1], name="out")

        self.w_hidden = tf.get_variable("w_hidden" + self.reuse * str(self.index),
                                        shape=[self.dimension, self.nb_neurons],
                               initializer=tf.contrib.layers.xavier_initializer())
        self.b_hidden = tf.get_variable("b_hidden" + self.reuse * str(self.index),
                                        [1, self.nb_neurons],
                               initializer=tf.constant_initializer(0.0))

        self.hidden_layer = tf.nn.tanh(tf.matmul(self.input, self.w_hidden) + self.b_hidden)

        self.w_out = tf.get_variable("w_out" + self.reuse * str(self.index),
                                     shape=[self.nb_neurons, 1],
                            initializer=tf.contrib.layers.xavier_initializer())
        self.b_out = tf.get_variable("b_out" + self.reuse * str(self.index), [1, 1],
                            initializer=tf.constant_initializer(0.0))

        self.y_hat = tf.matmul(self.hidden_layer, self.w_out) + self.b_out

        self.cost = tf.reduce_mean(tf.pow(self.y_hat - self.output, 2))
        self.train_step = tf.train.AdamOptimizer(self.learn_rate).minimize(self.cost)

        config = tf.ConfigProto(intra_op_parallelism_threads=4, inter_op_parallelism_threads=1)

        self.sess = tf.Session(config=config)

        self.save = save

        if self.save:
            self.saver = tf.train.Saver()



    def initialize_model(self):

        w_hidden = self.w_hidden
        b_hidden = self.b_hidden
        w_out = self.w_out
        b_out = self.b_out

        with self.sess.as_default():
            tf.global_variables_initializer().run()

        self.w_hidden = w_hidden
        self.b_hidden = b_hidden
        self.w_out = w_out
        self.b_out = b_out


    def train(self, data_class, max_epoch, batch_size, path):

        if not os.path.exists(path):
            os.makedirs(path)

        with self.sess.as_default():

            min_epoch = int(data_class.TrnInp.shape[0] / batch_size)

            for loop in range(max_epoch):

                batch_xs, batch_ys = data_class.next_batch(batch_size)
                self.sess.run(self.train_step, feed_dict={self.input: batch_xs,
                                                          self.output: batch_ys})

                if loop%min_epoch == 0:
                    self.error_track.append(self.sess.run(self.cost,
                                                      feed_dict={self.input: data_class.TstInp,
                                                                 self.output: data_class.TstOut}))

            if self.save:
                self.saver.save(self.sess, path + '/model.checkpoint')




    def restore(self, data, from_saver=False, **kwargs):

        with self.sess.as_default():

            if from_saver:
                path = kwargs['path']
                self.saver.restore(self.sess, path + '/model.checkpoint')

            self.SE = np.power(self.sess.run(self.y_hat,
                                               feed_dict={self.input: data[:,1:]}) - data[:,0].reshape(-1,1), 2)


        return self.SE

